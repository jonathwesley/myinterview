package com.example;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONArray;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.amazonaws.services.s3.model.Bucket;

/**
 * Create an implementation of a Rest API client.
 * Prints out how many records exists for each gender and save this file to s3 bucket
 * API endpoint=> https://3ospphrepc.execute-api.us-west-2.amazonaws.com/prod/RDSLambda 
 * AWS s3 bucket => interview-digiage
 *
 */
public class TASK4 {
	public static AmazonS3 getConnection() {
		List<List<String>> records = new ArrayList<>();
		try (BufferedReader br = new BufferedReader(new FileReader("src/main/resources/acesso.csv"))) {
		    String line;
		    while ((line = br.readLine()) != null) {
		        String[] values = line.split(",");
		        records.add(Arrays.asList(values));
		    }
		} catch (Exception e) {
			System.out.println("An error occurred. " + e);
		}
		
		AWSCredentials credentials = new BasicAWSCredentials(records.get(1).get(2), records.get(1).get(3));
		return AmazonS3ClientBuilder.standard().withCredentials(new AWSStaticCredentialsProvider(credentials)).withRegion(Regions.DEFAULT_REGION).build();
	}
	
	public static void uploadS3(String file_path, String bucket_name, String key_name) {
		System.out.println("Uploading " + file_path + " to S3 bucket " + bucket_name);
		
		AmazonS3 s3client = getConnection();
		try {
        	s3client.putObject(bucket_name, key_name, new File(file_path));
        } catch (AmazonServiceException e) {
            System.err.println(e.getErrorMessage());
            System.exit(1);
        }
	}
	
	public static void createBucket(String bucket_name) {
		System.out.println("Creating " + bucket_name + " bucket");
		
        AmazonS3 s3client = getConnection();
        if (s3client.doesBucketExistV2(bucket_name)) {
            System.out.println("Bucket " + bucket_name + " already exists");
        } else {
            try {
            	s3client.createBucket(bucket_name);
            } catch (AmazonS3Exception e) {
                System.err.println(e.getErrorMessage());
            }
        }
	}
	
    public static void main(String[] args) {
    	String keyName = "genders";
    	String fileName = "genders.txt";
    	String bucketName = "fileGenders";
    	
    	try {
            Client client = ClientBuilder.newClient();
            WebTarget webTarget = client.target("https://3ospphrepc.execute-api.us-west-2.amazonaws.com/prod/RDSLambda");
            Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);

            Response response = invocationBuilder.get();
            String output = response.readEntity(String.class);
            
            JSONArray jsonArray = new JSONArray(output);
            
            int maleCount = 0;
            int femaleCount = 0;
            for (int i = 0; i < jsonArray.length(); i++) {
            	String gender = jsonArray.getJSONObject(i).getString("gender");
            	if(gender.equals("M"))
            		maleCount += 1;
            	else
            		femaleCount += 1;
            }
            
            System.out.println("Male count: " + maleCount);
            System.out.println("Female count: " + femaleCount);
            
            File myObj = new File(fileName);
            if (myObj.createNewFile()) {
            	System.out.println("File created: " + myObj.getName());
            } else {
            	System.out.println("File already exists.");
            }
            
            FileWriter myWriter = new FileWriter(fileName);
            myWriter.write("Male count: " + maleCount + "\n" + "Female count: " + femaleCount);
            myWriter.close();
            
            createBucket(bucketName);
            uploadS3(fileName, bucketName, keyName);
            
        } catch (IOException e) {
  	      System.out.println("An error occurred. " + e);
  	    } catch (Exception e) {
          System.out.println("Exception in NetClientGet:- " + e);
        }
    }
}

