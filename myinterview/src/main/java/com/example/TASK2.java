package com.example;
import java.util.LinkedList;
/**
 * Task here is to write a list. Each element must know the element before and
 * after it. Print out your list and them remove the element in the middle of
 * the list. Print out again.
 *
 */
public class TASK2 {
    public static void main(String[] args) {
        // Doubly Linked List, as the java implementation of the Node contais previous and next pointers
        LinkedList<String> list = new LinkedList<String>();

        list.add("First element");
        list.add("Second element");
        list.add("Third element");

        System.out.println("Initial list: " + list);

        list.remove(1);

        System.out.println("Element removed: " + list);
    }
}