package com.example;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.Random;

/**
 * Write a list and add an aleatory number of Strings. In the end, print out how
 * many distinct itens exists on the list.
 *
 */
public class TASK3 {

    public static String getRandomString(int targetStringLength){
        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        Random random = new Random();
        StringBuilder buffer = new StringBuilder(targetStringLength);
        for (int i = 0; i < targetStringLength; i++) {
            int randomLimitedInt = leftLimit + (int) 
            (random.nextFloat() * (rightLimit - leftLimit + 1));
            buffer.append((char) randomLimitedInt);
        }
        String generatedString = buffer.toString();

        return generatedString;
    } 
    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<String>();

        int limit = 25;
        Random rand = new Random();
        int intRandom = rand.nextInt(limit) + 1;

        for(int i = 0; i < intRandom; i++){
            list.add(getRandomString(1));
        }

        Set<String> uniqueList = new HashSet<String>(list);

        System.out.println("List size: " + intRandom);
        System.out.println("List: " + list);
        System.out.println("Number of distinct itens: " + uniqueList.size());

    }
}
