package com.example;
/**
 * 
 *
 * Task here is to implement a function that says if a given string is
 * palindrome.
 * 
 * 
 * 
 * Definition=> A palindrome is a word, phrase, number, or other sequence of
 * characters which reads the same backward as forward, such as madam or
 * racecar.
 */
public class TASK1 {
     
    public static boolean isPalindrome(String word) {
        String clean = word.replaceAll("\\s+", "").toLowerCase();

        StringBuilder plain = new StringBuilder(clean);
        StringBuilder reverse = plain.reverse();

        return (reverse.toString()).equals(clean);
    }

    public static void main(String[] args) {
        String word = "racecar";

        if(isPalindrome(word))
            System.out.println("The given word is a palindrome");
        else
            System.out.println("The given word is not a palindrome");
    }

}